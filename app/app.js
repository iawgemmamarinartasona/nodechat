require("dotenv").config();
require("./io");

const port = process.env.SERVER_PORT || 6000;
var express = require("express"),
    app = express(),
    server = require("http").createServer(app),
    path = require("path"),
    mongoose = require('mongoose'),
    handlerError = require("./routes/handler"),
    session = require('express-session'),
    router = express.Router();

  app.use(session({ secret: 'yellow', resave: false, saveUninitialized: true }));
    app.use(router);

server.listen(port, (err, res) => {
    if (err) console.log(`ERROR: Connecting APP ${err}`);
    else console.log(`Server is running on port ${port}`);
});

// Import routes of our app

var socketsRouter = require("./routes/sockets");
//var handlerError = require("./routes/handler");
var index = require("./routes/home");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("path"));
app.use(express.static(path.join(__dirname, "public")));

// Define routes using URL path
app.use("/", index);
app.use(handlerError);

/*Socket functions */
//Load app dependencies


var http = require('http');

mongoose.connect(
  `mongodb://root:pass12345@mongodb:27017/tutorial?authSource=admin`,
  { useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
  }
);

module.exports = app;
const user = require('../models/user');
var User = require('../models/user'),
    session = require('express-session');

//Create a new user and save it
var add = (req, res) => {
    var user = new User({ name: req.body.name, password: req.body.password });
    user.save();
    console.log(user.name);
    res.redirect("/");
};

//find all people
var list = (req, res, next) => {
    User.find(function(err, users) {
        return users;
    });
};

//find person by id
var find = (req, res) => {
    User.findOne({ name: req.params.name }, function(error, user) {
        res.redirect("/list");
    })
};

var logout = (req, res) => {
    delete req.session.userId;
    res.redirect('/');
    res.json({message: 'loged out'});
}

var login = async (req, res) => {
    const user = await User.findOne({name: req.body.name});
    let sess=req.session;
    if (!user) {
        res.redirect("/register");
    }
    if (user.password != req.body.password) {
        res.redirect("/register");
    }
    sess.id = user._id;
    //res.json({message: 'loged in', username: user.username, password: user.password});
    res.redirect("/list");
}

var getNameUser = (req, res, next) => {
    let username = req.session.name;
    return "holasession";
}

module.exports = {
    add,
    list,
    find,
    login,
    logout,
    getNameUser
}
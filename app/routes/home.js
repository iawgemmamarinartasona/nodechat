var express = require("express"),
    path = require("path"),
    router = express.Router();

router.route("/").get(async(req, res, next) => {
    res.render("index2");
});
router.route("/chatView").post(async(req, res, next) => {
    console.log(req.body.chatId);
    res.render("chatView", {chatId: req.body.chatId});
});
router.route("/list").get(async(req, res, next) => {
    res.render("list");
});
router.route("/history").get(async(req, res, next) => {
    res.render("historyChat");
});
router.route("/historySelect").get(async(req, res, next) => {
    res.render("historySelect");
});
router.route("/register").get(async(req, res, next) => {
    res.render("register");
});


var ctrlDir = path.resolve("controllers/");
var userCtrl = require(path.join(ctrlDir, "user"));
router.post('/', userCtrl.login);
router.post('/register', userCtrl.add);
// router.post('/list', userCtrl.select);


module.exports = router;